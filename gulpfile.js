var gulp = require('gulp'),
	sass = require('gulp-ruby-sass'),
	uglify = require('gulp-uglify'),
	manifest = require('gulp-manifest'),
    concat = require('gulp-concat'),
    ignore = require('gulp-ignore'),
	browserSync = require('browser-sync').create(),
    config = require('./config').config,
    path = require('path');
var srcDir = __dirname +'/src/';
var destDir = __dirname+'/dist/';



var scripts = [];
if(config.js){
    config.js.forEach(function(item){
        scripts.push(srcDir+'js/'+item);
    });
}

gulp.task('sass', function(){
	return sass(srcDir+'css/*.scss',{style:'compressed'})
	.pipe(gulp.dest(destDir+'css'))
	.pipe(browserSync.stream({match: '**/*.css'}));
});
gulp.task('scripts', function(){
	return gulp.src(scripts)
    .pipe(concat('main.min.js'))
    .pipe(uglify())
	.pipe(gulp.dest(destDir+'js'));
});
gulp.task('scripts-watch',['scripts'],browserSync.reload);
function excludeCopy(file){
    var ext = path.extname(file.path);

    if (ext == ".scss" || ext == ".js") {
        return true;
    } else {
        return false;
    }
}
gulp.task('copy', function(){
   return gulp.src(srcDir+'**')
       .pipe(ignore.exclude(excludeCopy))
       .pipe(gulp.dest(destDir));
});
function excludeWhen(file) {
    var fileName = path.relative(srcDir, file.path);
    var ext = path.extname(file.path);

    if (ext == ".scss" || ext == ".js" || fileName.match(/cache.manifest/)) {
        return true;
    } else {
        return false;
    }
}
gulp.task('manifest', function () {
    return gulp.src(destDir + '**')
    .pipe(ignore.exclude(excludeWhen))
    .pipe(manifest({
        hash: true,
        preferOnline: true,
        network: ['http://*', 'https://*', '*'],
        filename: 'cache.manifest'
    }))
    .pipe(gulp.dest(destDir));
});
gulp.task('default',function(){
	gulp.start('sass','scripts','copy');
});
gulp.task('watch', ['sass','scripts','copy'],function(){
	browserSync.init({
		server: {
			baseDir: [destDir,'node_modules']
		}
	});
	gulp.watch(srcDir+'css/*.scss',['sass']);
    gulp.watch(srcDir+'js/*.js',['scripts-watch']);
    gulp.watch(srcDir+'**',['copy']);
	gulp.watch(destDir+"*.html").on("change", browserSync.reload);
});
