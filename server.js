var express = require('express');
var app = express();
var server = require('http').Server(app);

app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'X-Requested-With');
  next();
});

app.use(express.static(__dirname + '/dist'));
app.use(express.static(__dirname + '/node_modules'));

server.listen(process.env.PORT||80);