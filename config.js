var config = {
    js:[
        'vendor/angular.min.js',
        'vendor/angular*.js',
        'vendor/*.js',
        'main.js']
};
if(exports){
    exports.config = config;
}