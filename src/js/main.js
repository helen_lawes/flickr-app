angular.module('flickrApp',['ui.router','flickrApp.service','flickrApp.filters','flickrApp.directives'])
.run(['$rootScope', '$state', '$stateParams', function ($rootScope, $state, $stateParams) {
	$rootScope.$state = $state;
	$rootScope.$stateParams = $stateParams;
}])
.config(['$stateProvider', '$urlRouterProvider',function($stateProvider,$urlRouterProvider){
    $urlRouterProvider.otherwise('/');
    
    $stateProvider
        .state('list', {
            url: '/',
            templateUrl: 'tpl/list.html',
            resolve:{
                listData:['dataService',function(dataService){
					// retrieve the initial data to display before view loads
                    return dataService.fetch();
                }]
            },
            controller:['$scope','listData','dataService',function($scope,listData,dataService){
                $scope.list = listData;
				$scope.search = '';

				// reload the list with the search term
				$scope.refreshList = function(){
					dataService.newTags($scope.search).then(function(data){
						$scope.list = data;
					});
				};
				// fetch more results to add to the list
				$scope.loadMore = function(){
					dataService.fetch().then(function(data){
						// the list data is handled in the service so override the scope variable
						$scope.list = data;
					});
				};
            }]
        })
        .state('list.detail',{
            url:'detail/:id',
            templateUrl: 'tpl/detail.html',
			resolve:{
				itemData:['dataService','$stateParams',function(dataService,$stateParams){
					return dataService.getDetail($stateParams.id);
				}]
			},
			controller:['$scope','itemData',function($scope,itemData){
				$scope.item = itemData;
				// the tags are returned with spaces, split them to be able to add a link out for each one
				if(itemData && itemData.tags != ""){
					$scope.tags = itemData.tags.split(' ');
				}
			}]
        })
}]);

angular.module('flickrApp.service',[])
.factory('dataService',['$http','$q',function($http,$q){
   return {
	   data: [],
	   ids: [],
	   tags:'',
       url: 'https://api.flickr.com/services/feeds/photos_public.gne?format=json&jsoncallback=JSON_CALLBACK',
       fetchData: function(){
		   // return just the photo items from the flickr feed, sending the search as the tags parameter
		   return $http.jsonp(this.url,{params:{tags:this.tags}}).then(function(resp){
			   return resp.data.items;
           },function(err){
			   return err;
		   });
       },
	   fetch: function(){
		    var d = $q.defer();
			var factory = this;
			this.fetchData().then(function(items){
				// once the data is returned check for duplicates by base64 encoding the url which is unique and adding if to an ids array, if the id is found in the ids array don't return it
				angular.forEach(items,function(value){
					value.id = btoa(value.link);
					if(factory.ids.indexOf(value.id) == -1){
						factory.ids.push(value.id);
						factory.data.push(value);
					}
				});
				d.resolve(factory.data);
			});
		   return d.promise;
	   },
	   getDetail: function(id){
		   // return a specific photo item using the generated base64 id
		   for(var i = 0; i < this.data.length; i++){
			   if(id == this.data[i].id){
				   return this.data[i];
			   }
		   }
		   return null;
	   },
	   newTags: function(tags){
		   // reset the currently stored data and ids, then fetch the feed with the search tags
		   this.tags = tags;
		   this.data = [];
		   this.ids = [];
		   return this.fetch();
	   }
   } 
}]);

angular.module('flickrApp.filters',[])
.filter('extractAuthor',function(){
    return function(input){
		// the author's name is contained within brackets
		// if brackets are found return the contents within
        if(input.match(/\(/)){            
            var pieces = input.split('(');
            return pieces[1].replace(/\)/,'');
        }
        return input;
    }
})
.filter('untitled',function(){
	return function(input){
		// check for photos without titles and return untitled to help with consistent layout
		if(input.trim() == ""){
			return '<span class="no-title">Untitled</span>';
		}
		return input;
	}
})
.filter('trustAsHtml',['$sce',function($sce){
	return function(input){
		// the untitled filter returns html which needs to be trusted to be inserted into the heading
		return $sce.trustAsHtml(input);
	}
}]);

angular.module('flickrApp.directives',[])
.directive('scrollLoad',function(){
	return function($scope, elm){
		var raw = elm[0];
		elm.on('scroll', function(){
			// when near the bottom of the page after a scroll load more photos from the feed
			if(raw.scrollTop + raw.offsetHeight >= raw.scrollHeight){
				$scope.loadMore();
			}
		});
	}
});