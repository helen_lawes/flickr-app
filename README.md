# README FOR FLICKR APP #

This is an app built to display the flickr public photo feed. It is responsively designed to fit most screen devices. It is built on the AngularJS (v1.5.0) and AngularJS UI Router (v0.2.18). The app allows searching for photos by tags and further results are shown, if any are returned, upon reaching near the end of the page.

### How do I get set up? ###

The fully built application can be found in the dist/ folder. 

To develop or build locally, Node.js is required and the modules in the package.json file will need to be installed by the Node.js package manager. It is also best to have gulp installed globally to run the commands in the gulpfile.

To build run:

```
#!js

gulp
```

To develop and watch for file changes run:

```
#!js

gulp watch
```
This will also launch the website in the browser. Browser sync is enabled so any changes made to the files will cause the browser window to reload.